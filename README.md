# php-amqpbus #

This library is a wrapper for php-amqplib.

You can use Consumer and Publisher/Producer in your projects to process messages from RabbitMQ by AMQP.

## Supported RabbitMQ Version ##
This library uses `AMQP 0.9.1` by default and thus requires [RabbitMQ 2.0 or later version] (http://www.rabbitmq.com/download.html).


## Setup ##

 Add install via composer:

```bash
$ composer.phar require "evgmel/php-amqpbus:dev-master"
```

## Publisher and Consumer Usage Example ##

Before using these examples you should setup RebbitMQ and need to know settings parameters to create 
instances of consumer and producer.
### Usage in Silex application:
```php

// publisher.php file
 
<?php 
use Bus\AMQP\Publisher\MessagePublisherInterface;
require_once __DIR__ . '/../vendor/autoload.php';
/** @var \Silex\Application $app */
$app = require_once(__DIR__ . '/../app/config/bootstrap/app.php');
require_once(__DIR__ . '/../app/config/conf_dev.php');
$app->boot();
/** @var MessagePublisherInterface $publisher */
$publisher = $app['app.bus.amqp.publisher'];
$eventName = $argv[1] ?? 'not_specified_event_name';
$data = isset($argv[2]) ? [$argv[2]] : ['defaultDataKey' => 'defaultDataValue'];
$publisher->publish($eventName, $data);
```

Run this command:
```bash
    php7.1 publisher.php myEventNameHere MyMessageTextHere
```
After previous command a message with name 'myEventNameHere' will be published to the queue;


```php
 // consumer.php file
 
 <?php
use Bus\AMQP\Consumer\MessageConsumerInterface;
require_once __DIR__ . '/../vendor/autoload.php';
/** @var \Silex\Application $app */
$app = require_once(__DIR__ . '/../app/config/bootstrap/app.php');
require_once(__DIR__ . '/../app/config/conf_dev.php');
$app->boot();
/** @var MessageConsumerInterface $consumer */
$consumer = $app['app.bus.amqp.consumer'];

register_shutdown_function([$consumer, 'handleError']);

$consumer->consume();
```
Run this command:
```bash
    php7.1 consumer.php 
```
Consumer will process incoming messages from a queue.