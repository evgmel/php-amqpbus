<?php
namespace Bus\AMQP\Consumer;

use Bus\AMQP\Enum\AMQPExchangeType;
use Bus\AMQP\Event\Entity\MessageEvent;
use Bus\AMQP\Event\Entity\ProcessEvent;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MessageConsumer implements MessageConsumerInterface
{
    /** @var AMQPStreamConnection  */
    protected $connection;
    /** @var EventDispatcherInterface  */
    protected $eventDispatcher;
    /** @var AMQPChannel  */
    protected $channel;
    /** @var string  */
    protected $exchange;
    /** @var  string */
    protected $queuePostfix;
    /** @var  string */
    protected $exchangeType;
    /** @var  string */
    protected $consumerTag;
    /** @var  AMQPMessage */
    protected $lastMessage;
    /** @var  string */
    protected $lastMessageName;
    /** @var  string */
    protected $lastMessageMeta;
    /** @var  array */
    protected $params;

    public function __construct(
        AMQPStreamConnection $connection,
        EventDispatcherInterface $eventDispatcher,
        string $exchange,
        string $queuePostfix,
        string $exchangeType = AMQPExchangeType::TYPE_DIRECT,
        array $params = []
    )
    {
        $this->connection       = $connection;
        $this->eventDispatcher  = $eventDispatcher;
        $this->exchange         = $exchange;
        $this->queuePostfix     = $queuePostfix;
        $this->exchangeType     = $exchangeType;
        $this->params           = $params;
    }

    /**
     * Starts to consume messages
     * @return void
     */
    public function consume()
    {
        $this->initChannel();

        $channel = $this->getChannel();

        echo PHP_EOL . "Consumer started..." . PHP_EOL;

        while (count($channel->callbacks)) {
            $channel->wait();
        }
    }

    /**
     * @param AMQPMessage $message
     * @return void
     */
    public function processMessage(AMQPMessage $message)
    {
        $this->lastMessage = $message;

        if ($data = $this->extractData($message)) {
            list($messageName, $args, $meta) = $data;

            $this->lastMessageName = $messageName;
            $this->lastMessageMeta = $meta;

            $this->eventDispatcher->dispatch($messageName, new MessageEvent($messageName, $args, $meta));

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);

            $this->eventDispatcher->dispatch(
                ProcessEvent::EVENT_MESSAGE_HANDLED,
                new ProcessEvent($messageName, $meta, $message)
            );
        } else {
            $this->handleError();
        }
    }

    /**
     * Set configuration parameter
     *
     * @param string $key
     * @param bool $value
     *
     * @return MessageConsumerInterface
     */
    public function setParam(string $key, bool $value)
    {
        $this->params[$key] = $value;

        return $this;
    }

    /**
     * Default error handler
     *
     * @return void
     */
    public function handleError()
    {
        $this->eventDispatcher->dispatch(
            ProcessEvent::EVENT_MESSAGE_HANDLE_ERROR,
            new ProcessEvent(
                $this->lastMessageName ?? ProcessEvent::MESSAGE_NAME_NOT_PARSED,
                $this->lastMessageMeta ?? [],
                $this->lastMessage ?? new AMQPMessage()
            )
        );

        if ($this->lastMessage) {
            $this->lastMessage->delivery_info['channel']
                ->basic_ack($this->lastMessage->delivery_info['delivery_tag']);
        }
    }

    /**
     * Extract data from message
     *
     * @param AMQPMessage $message
     * @return array|null
     */
    protected function extractData(AMQPMessage $message)
    {
        $data = null;

        $body = unserialize($message->getBody());

        if (!empty($body)) {
            $data = [];
            $data[0] = $body[0] ?? ProcessEvent::MESSAGE_NAME_NOT_PARSED;
            $data[1] = $body[1] ?? [];
            $data[2] = $body[2] ?? [];
        }

        return $data;
    }

    /**
     * Initializes channel parameters
     *
     * @return void
     */
    protected function initChannel()
    {
        $channel = $this->getChannel();
        $this->channel->exchange_declare(
            $this->exchange,
            $this->exchangeType,
            $this->params['passive']        ?? false,
            $this->params['durable']        ?? true,
            $this->params['auto_delete']    ?? false,
            $this->params['internal']       ?? false,
            $this->params['nowait']         ?? false,
            $this->params['arguments']      ?? null,
            $this->params['ticket']         ?? null
        );

        $channel->queue_declare(
            $this->getQueueName(),
            $this->params['passive']        ?? false,
            $this->params['durable']        ?? true,
            $this->params['exclusive']      ?? false,
            $this->params['auto_delete']    ?? false,
            $this->params['nowait']         ?? false,
            $this->params['arguments']      ?? null,
            $this->params['ticket']         ?? null
        );

        $channel->queue_bind($this->getQueueName(), $this->exchange, $this->getRoutingKey());

        $channel->basic_consume(
            $this->getQueueName(),
            $this->getConsumerTag(),
            $this->params['no_local']       ?? false,
            $this->params['no_ack']         ?? false,
            $this->params['exclusive']      ?? false,
            $this->params['nowait']         ?? false,
            $this->params['callback']       ?? [$this, 'processMessage'],
            $this->params['ticket']         ?? null,
            $this->params['arguments']      ?? null
        );
    }

    /**
     * Gets queue name
     *
     * @return string
     */
    protected function getQueueName():string
    {
        return $this->exchange . $this->queuePostfix;
    }

    /**
     * Gets routing key
     *
     * @return string
     */
    protected function getRoutingKey():string
    {
        return $this->exchange . '.#';
    }

    /**
     * Gets AMQ consumer tag
     *
     * @return string
     */
    protected function getConsumerTag():string
    {
        if (null === $this->consumerTag) {
            $this->consumerTag = 'PHP_' . getmypid();
        }

        return $this->consumerTag;
    }

    /**
     * @return AMQPChannel
     */
    protected function getChannel():AMQPChannel
    {
        if ($this->channel === null) {
            $this->channel = $this->connection->channel();
        }

        return $this->channel;
    }

}
