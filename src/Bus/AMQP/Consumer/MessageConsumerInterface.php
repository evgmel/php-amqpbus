<?php
namespace Bus\AMQP\Consumer;

use PhpAmqpLib\Message\AMQPMessage;

interface MessageConsumerInterface
{
    /**
     * Starts to consume messages
     * @return void
     */
    public function consume();

    /**
     * Processes message from queue
     *
     * @param AMQPMessage $message
     * @return void
     */
    public function processMessage(AMQPMessage $message);

    /**
     * Default error handler
     *
     * @return void
     */
    public function handleError();

    /**
     * Set configuration parameter
     *
     * @param string $key
     * @param bool $value
     *
     * @return MessageConsumerInterface
     */
    public function setParam(string $key, bool $value);
}