<?php
namespace Bus\AMQP\Event\Entity;

use Symfony\Component\EventDispatcher\GenericEvent;

class MessageEvent extends GenericEvent
{
    protected $meta;

    public function __construct($subject = null, array $arguments = [], array $meta = [])
    {
        parent::__construct($subject, $arguments);

        $this->meta = $meta;
    }

    public function getMeta()
    {
        return $this->meta;
    }
}
