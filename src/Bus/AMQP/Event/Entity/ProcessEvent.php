<?php
namespace Bus\AMQP\Event\Entity;

use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\EventDispatcher\Event;

class ProcessEvent extends Event
{
    const
        MESSAGE_NAME_NOT_PARSED     = 'message_name.not_parsed',

        EVENT_MESSAGE_HANDLED       = 'amqp.event.message_handled',
        EVENT_MESSAGE_HANDLE_ERROR  = 'amqp.event.message_handle.error';

    protected $messageName;
    protected $meta;
    protected $message;

    public function __construct(string $messageName, $meta, AMQPMessage $message)
    {
        $this->messageName = $messageName;
        $this->meta        = $meta;
        $this->message     = $message;
    }

    public function getMessageName():string
    {
        return $this->messageName;
    }

    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @return AMQPMessage
     */
    public function getAmqpMessage():AMQPMessage
    {
        return $this->message;
    }
}
