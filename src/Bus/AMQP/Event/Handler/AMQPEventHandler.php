<?php
namespace Bus\AMQP\Event\Handler;

use Psr\Log\LoggerInterface;
use Bus\AMQP\Event\Entity\ProcessEvent;

class AMQPEventHandler
{
    /** @var LoggerInterface  */
    private $logger;
    /** @var string  */
    private $env;

    public function __construct(LoggerInterface $logger, string $env)
    {
        $this->logger = $logger;
        $this->env    = $env;
    }

    public function onAMQPMessageHandled(ProcessEvent $event)
    {
        if (($this->env <=> 'prod') !== 0) {
            echo PHP_EOL . "Message is processed: {$event->getMessageName()}" . PHP_EOL;
        }

        $this->logger->info("game.consumer.message.handle.success", ['message_name' => $event->getMessageName()]);
    }

    public function onAMQPMessageHandleError(ProcessEvent $event)
    {
        if (($this->env <=> 'prod') !== 0) {
            echo PHP_EOL . "Couldn't parse message: {$event->getMessageName()}" . PHP_EOL;
        }

        if ($event->getMessageName() === ProcessEvent::MESSAGE_NAME_NOT_PARSED) {
            $errorData['reason'] = "Couldn't parse message name during data extraction";
        } else {
            $errorData['reason'] = "Unknown error happened during data consuming";
        }

        $errorData['message_name'] = $event->getMessageName();
        $errorData['message_body'] = $event->getAmqpMessage()->getBody();
        $errorData['error'] = error_get_last();

        $this->logger->error("game.consumer.error", $errorData);
    }
}
