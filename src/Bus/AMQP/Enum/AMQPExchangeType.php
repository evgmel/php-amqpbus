<?php
namespace Bus\AMQP\Enum;

/**
 * For more information about types
 * @see https://www.rabbitmq.com/tutorials/amqp-concepts.html
 */
class AMQPExchangeType
{
    const
        TYPE_DIRECT     = 'direct',
        TYPE_FANOUT     = 'fanout',
        TYPE_HEADERS    = 'headers',
        TYPE_TOPIC      = 'topic';
}