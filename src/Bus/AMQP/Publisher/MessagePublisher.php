<?php
namespace Bus\AMQP\Publisher;

use Bus\AMQP\Enum\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;
use \PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class MessagePublisher implements MessagePublisherInterface
{
    /** @var AMQPStreamConnection  */
    protected $connection;
    /** @var string  */
    protected $exchange;
    /** @var  string */
    protected $exchangeType;
    /** @var AMQPChannel  */
    protected $channel;
    /** @var  array */
    protected $params;

    public function __construct(
        AMQPStreamConnection $connection,
        string $exchange,
        string $exchangeType = AMQPExchangeType::TYPE_DIRECT,
        array $params = []
    )
    {
        $this->connection       = $connection;
        $this->exchange         = $exchange;
        $this->exchangeType     = $exchangeType;
        $this->params           = $params;
    }

    /**
     * Set configuration parameter
     *
     * @param string $key
     * @param bool $value
     *
     * @return MessagePublisherInterface
     */
    public function setParam(string $key, bool $value)
    {
        $this->params[$key] = $value;

        return $this;
    }

    /** Publishes a message to queue specified in constructor params
     *
     * @param string $messageName
     * @param array $data
     * @param array $meta
     *
     * @return void
     */
    public function publish(string $messageName, array $data, array $meta = [])
    {
        $message = $this->prepareMessage($messageName, $data, $meta);

        $this->queueMessage($message, $messageName);
    }

    protected function queueMessage(AMQPMessage $message, string $messageName)
    {
        $this->getChannel()->basic_publish($message, $this->exchange, $this->buildRoute($messageName));
    }

    /**
     * Prepares a message to be sent
     *
     * @param string $messageName
     * @param array $data
     * @param array $meta
     *
     * @return AMQPMessage
     */
    protected function prepareMessage(string $messageName, array $data, array $meta = []):AMQPMessage
    {
        $message = new AMQPMessage(
            serialize([$messageName, $data, $meta]),
            ['content_type' => 'text/plain']
        );

        return $message;
    }

    /**
     * Builds queue name from specified message name and exchange prefix
     *
     * @param string $messageName
     * @return string
     */
    protected function buildRoute(string $messageName):string
    {
        return $this->exchange . '.' . $messageName;
    }

    /**
     * @return AMQPChannel
     */
    protected function getChannel():AMQPChannel
    {
        if ($this->channel === null) {
            $this->channel = $this->connection->channel();
            $this->channel->exchange_declare(
                $this->exchange,
                $this->exchangeType,
                $this->params['passive']        ?? false,
                $this->params['durable']        ?? true,
                $this->params['auto_delete']    ?? false,
                $this->params['internal']       ?? false,
                $this->params['nowait']         ?? false,
                $this->params['arguments']      ?? null,
                $this->params['ticket']         ?? null
            );
        }

        return $this->channel;
    }
}