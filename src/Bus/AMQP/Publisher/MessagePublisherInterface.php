<?php
namespace Bus\AMQP\Publisher;


interface MessagePublisherInterface
{
    /** Publishes a message to queue specified in constructor params
     *
     * @param string $messageName
     * @param array $data
     * @param array $meta
     *
     * @return void
     */
    public function publish(string $messageName, array $data, array $meta = []);

    /**
     * Set configuration parameter
     *
     * @param string $key
     * @param bool $value
     *
     * @return MessagePublisherInterface
     */
    public function setParam(string $key, bool $value);
}